class AddAttachmentToImage < ActiveRecord::Migration
  def change
    add_column :images, :img, :attachment
  end
end
