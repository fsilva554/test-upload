class ImagesController < ApplicationController
  def create
    @img = Image.create(image: params[:image][:file])
    render json: { response: 'Ok', url: @img.image.url }
  end
end
