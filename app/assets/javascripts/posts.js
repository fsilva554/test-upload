$(document).ready(function(){

  $("#post-body").markdown({
  additionalButtons: [
    [{
      name: 'group1',
      data: [{
        name: 'uploadImage',
        icon: 'glyphicon glyphicon-glass',
        title: 'Upload',
        callback: function(e) {
          $("#myModal").modal('toggle').on('hide.bs.modal', function(e){
            $("#post-body").data('markdown').replaceSelection('Hello there!'); 
          });
        }
      }]
    }]
  ]});
 
  $('form').bind('ajax.remotipartSubmit', function(event, xhr, settings){
    settings.dataType = "html *";
  });

  $('form').bind("ajax:success", function(event, data, status, xhr){
   
    // How to get data out of the response
    console.debug(JSON.parse(xhr.responseText).url);
    $("#post-body").data('markdown').replaceSelection("![enter image description here]" + JSON.parse(xhr.responseText).url +  " 'title'");
    //if ( $(this).data('remotipartSubmitted') ){
      //alert('helo');
    //}
  });

});
